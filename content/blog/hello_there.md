+++
title = "Hello there."
date = "2025-01-25"
[extra]
author = "Hugo Mechiche"
authorLink = "https://hugo-mechiche.com"
+++
Hello there.
<!-- more -->
It's been a long time since I've posted here but guess what? I'm back!
Nothing much to add lol.

{{ centered_image(path="images/hello_there.jpg", alt="'Hello there' meme from Star Wars") }}
