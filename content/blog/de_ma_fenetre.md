+++
title = "De ma fenêtre"
date = "2022-09-15"
[extra]
author = "Hugo Mechiche"
authorLink = "https://hugo-mechiche.com"
+++
De ma fenêtre j'entends,  
le mouvement de ces femmes et hommes,  
De ma fenêtre j'observe,  
les toits pendant que les gens dorment,  
<!-- more -->  
Cerveau éteint et pourtant si bruyant,  
Je ne pige rien et pourtant j'suis conscient,  
conscient que ma vie ne mène à rien,  
conscient qu'avec personne je n'ai de lien,  
  
Le jour, j'écoute les pas des quidams,  
la nuit, j'étudie les hauteurs de Paname,  
le jour, je perçois les bruits des transports,  
la nuit, je scrute Saint-Michel et sa toison d'or,  
  
De ma fenêtre j'entends,  
le mouvement de ces femmes et hommes,  
De ma fenêtre j'observe,  
les toits pendant que les gens dorment.
