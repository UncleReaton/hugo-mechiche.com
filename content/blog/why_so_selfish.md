+++
title = "Why so selfish?"
date = "2020-10-13"
[extra]
author = "Hugo Mechiche"
authorLink = "https://hugo-mechiche.com"
+++
As you may know, online privacy and cyber-security are really important to me. So, obviously, I'm trying to spread awareness around me. When I started to do that, I assumed that my friends were going to be concerned and follow me (at least a little bit). What a surprise it has been when they told me "I don't give a damn about my data privacy", "my data are being hold by big companies since a long time and it haven't change my life so I don't think it's a really big deal." or "I'm not going to encrypt my communications, it's useless"
<!-- more -->
What you must know is that since encryption must be done for each communicant you are NOT doing it JUST FOR YOU. By encrypting your communications you're protecting yourself but also every person you're communicate with. Here is a little [tutorial](https://emailselfdefense.fsf.org/en/) to learn how to encrypt your emails. But you can also use some apps on your phone to send encrypted messages, it's really easy to use : you install it, you use it. Here is a few apps: Signal, Session (by Loki Foundation) and Telegram. _(you choose which app you want to trust to encrypt your communications)_.

As I just showed you, it's really easy to encrypt your communications. So... Why don't you want to use encryption ? Is it because you're "afraid" to change your habits ? Is it because none of your friends use it ?

If you're afraid to change your habits you must make that first little step that will make you a regular user of encryption very quickly. If it's about the lack of "friend user" you must help them to make the transition with you, you must spread awareness as much as possible and slowly you'll have new "friend user". You can share this article, the tutorial linked above or another source of information about data privacy and encryption.
