+++
title = "How to get rid of Google"
date = "2021-12-29"
updated = "2023-04-09"
[extra]
author = "Hugo Mechiche"
authorLink = "https://hugo-mechiche.com"
+++
It's not a secret -- except for people living under a rock --, Google is collecting data about everything on everyone. But we can do something about it! Do you really want Google to make money with **your** data? If you don't want any of this, if you want to regain a bit of your online privacy/freedom, come aboard matey and let's become a Pirate!  
<!-- more -->
## How to get rid of Google Chrome?
That's the easiest thing ever. Uninstall Chrome and install [Firefox](https://www.mozilla.org/en-US/firefox/new/) instead! You may have some memories about Firefox being slower than Chrome but that's not the case anymore, give it a try! Also don't forget to add some useful add-ons like [Ublock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/), [Privacy Badger](https://addons.mozilla.org/en-US/firefox/addon/privacy-badger17/) and [HTTPS Everywhere](https://addons.mozilla.org/en-US/firefox/addon/https-everywhere/). That's the minimum to be less tracked on Internet.
If you want something even more privacy-friendly, you can use [Tor Browser](https://www.torproject.org/download/) and use onion services as much as possible!

## How to get rid of the Google search engine?
With more than 90% of the Worlwide market share of search engines, Google rules over everything. Today, you don't even make a research on the Internet anymore, you are *googling* something. But do you really want to have every single one of your researches saved in a Google datacenter? And then sold to an advertiser to target you better?  
### What can we consider a good alternative?
That's a good question! Because I'm talking about Google but Bing, Yahoo! or even Yandex are doing the same thing (making profit out of you). So how should we define what's a good alternative?  
Let's have 2 different grades:  
1. [Proprietary](https://en.wikipedia.org/wiki/Proprietary_software) engine but without trackers  
2. [FLOSS](https://en.wikipedia.org/wiki/Free_and_open-source_software) (and self-hostable) engine

#### 1. [DuckDuckGo](https://duckduckgo.com/) (DDG)
*Search the web without being tracked* is the "slogan" of DDG.
You might know this engine but have you ever used it? 
It returns good search results. DDG claims to not track its user: ["we do not log (store) [information about your computer] at all. This is a very unusual practice, but we feel it is an important step to protect your privacy."](https://duckduckgo.com/privacy).  
Should you trust it? It depends, as you cannot verify if it really doesn't log information about you, you cannot be sure. But it seems quite trusted by the privacy-concerned community.

**Good to know**: DDG is available over Tor via its own [onion service](https://duckduckgogg42xjoc72x3sjasowoarfbgcmvfimaftt6twagswzczad.onion/)!

#### 2.
#### - [Whoogle](https://whoogle.sdf.org/)
Whoogle is basically a privacy friendly front-end to Google. ["Get Google search results, but without any ads, javascript, AMP links, cookies, or IP address tracking."](https://github.com/benbusby/whoogle-search) is the first thing you can read on its github README. You can self-host it but a lot of *good* websites/associations/individuals are hosting it so you don't have to bother with setting up a server. :)  
#### - [Searx](https://searx.fmac.xyz/)
[Searx](https://searx.github.io/searx/) is a meta search engine, which means it gets its results from different search engine (such as Google, Yandex, ...) but like Whoogle, it acts as "front-end" so you are not tracked by those search engines. So you just need to trust the entity who host the instance you're using and it's good or even better: Self-host your own instance! ;)  

**Good to know**: Both solutions have a Tor support!  
  
### How to set one of those as your default search engine (for Firefox)?
To set DDG as the default search engine just go to [about:preferences#search](about:preferences#search) and select DDG.  
For Whoogle or Searx, you can check it out [right here](https://support.mozilla.org/en-US/kb/add-or-remove-search-engine-firefox)  
  
## How to get rid of your Google account? (Gmail, Youtube, Google Drive, ...)
I ain't gonna lie, that's gonna be the most difficult part. But if you don't want to be part of a system where you are the product, that's absolutely necessary.  
### First step: Find a new email service provider
There are many associations which respect your privacy that provide an email service. For example you have [Disroot](https://disroot.org) or some [CHATONS](https://www.chatons.org/). If you have the soul of a real pirate, you can still self-host your own email service. :)
Now that you have your new email address, tell your friends and other services that need your address about this change.
### Second step: Get everything you have on Google Drive out of their *cloud*
*"There is no cloud, just someone else’s computer"* so if you ~still~ think your documents are safe from Google, well... You're wrong. :/  
Take back everything you have on their cloud service and save it on your own computers (not the one you use daily), or on someone's computer you trust. Again, some [CHATONS](https://www.chatons.org/) provide a cloud service. To set up your own cloud, check out [nextcloud](https://nextcloud.com/).
### Last step: Delete your Google account
Everything should be good now, except for android user except for Android users for whom it can be a bit more tricky (you can still check info about [lineageOS](https://lineageos.org/) for example), so don't wait any longer and go DELETE YOUR DAMNED GOOGLE ACCOUNT.
