+++
title = "My view on internet privacy"
date = "2021-07-04"
[extra]
author = "Hugo Mechiche"
authorLink = "https://hugo-mechiche.com"
+++
I'm young. I was born in an era where Internet was already everywhere, mostly used by companies or tech enthusiast. Lucky me, my father was --and still is-- a tech enthusiast so I had a computer at home. The first thing he taught me before letting me use it was how to protect me --as much as I possibly could at the time-- on the Internet "Do not ever reveal either your location, your name or even your age!" (For those wondering, yes, my use was strongly monitored. I wasn't free roaming in this gigantic space alone). And while I was amazed by the power of the World Wide Web, I was afraid on how it could become harmful for me, as a person.
<!-- more -->
I've always admired pirates (those on the sea) and cowboys; not because of the illegal side of their activities but because of the liberty they had. No one's looking above their shoulder at any time (or at least most of the times :D). And growing up, I kind of had this feeling by browsing the web. Then I started being interested in cyber-security. I've discovered that looking above the shoulder of someone is kind of simple so it made a bit more precautious with my use of the Internet. But it wasn't really much and I wasn't as scared as I was when I discovered Internet since I was thinking "Online problems aren't a big deal in real life". Until Snowden revelations \[a\]. I became afraid again but in a stronger way than when I was a little boy. I wasn't afraid of some possible creeps anymore. But afraid of corporations. Afraid of governments, even my own. It might be because I've always been in search of freedom and I realized that I had my ocean, my wild wild west taken away. Where I once thought I was the freest was in fact the place where I was the most watched over.

I'm not here to tell you what you should do. I'm here to tell you why privacy, online privacy, is something I care about.

### What is liberty?

Liberty is "the condition of being free from oppressive restriction or control by a government or other power" \[b\].

### What is privacy?

Privacy is "someone's right to keep their personal matters and relationships secret" \[c\]. This definition works for 'in real life'(IRL) privacy but also for online privacy because they are the same thing, they are the two sides of the same coin.

Therefore, privacy is a form of liberty.

## If You Have Nothing to Hide, You Have Nothing to Fear

This sentence/slogan appears on the nsa webpage about national domestic surveillance \[d\].

Well, I don't believe in this sentence. Let's take an IRL example: in my house I'm not doing anything illegal and I don't walk around naked neither; However, I do not want to let everybody watch what's going on through my windows so I have curtains. I hide something that I don't really have to hide and I bet you're doing the same. But why do I feel the need to hide the inside of the most important place for me ? For the same reason that we all have a lock on our doors, I want to feel safe and secure. It's exactly the same thing online. Do I want to have my government, google or event my neighbor to know what I searched this morning ? Not at all and yet it was just a pancake recipe (Yes, I just revealed my research from this morning, but I chose to do so). So even if I --theoretically-- have nothing to hide, I hide a lot of things. Does it mean that I should fear the government? Should I fear to be considered as a malicious person? Should I fear being watched?

No, I shouldn't, but I do. Why, you may ask? Because the surveillance is global. It is not targeted. How can it respects privacy? Again, let's transpose this to an IRL situation: Should I be ok that the government, to arrest a terrorist, searches my whole house? Without any warrant? No I shouldn't.

## But is it just about me?

Again, no. Global Surveillance is a threat to every individuals, even the ones who are not using Internet. Let's say that in my google calendar I have marked for the 14th of July that, with my friends Bob and Alice, I'll be watching the french military parade on the Champs-Élysées. Then, Google knows what I'm doing, where I'm doing it and with whom I'm doing it. So it also knows what Bob is doing on that day, what Alice is doing on that day. And I gave an example where it's kind of easy to know what information you're sharing. So when I'm watched, it doesn't only affecting me, it's affecting _all_ my relatives.

## Am I irreproachable?

I'm far from perfect. Firstly, I used GAFAM services for more than a decade. And still, now that I know all of this (and I don't know that much), I do not have the perfect behavior on the Internet. I surely still share some informations, even more than I imagine.

## Conclusion

To conclude, I value privacy because I consider it intrinsic to the notion of liberty. To me, losing it would simply mean losing our liberty. Regarding human actions on this subject, I think that, instead of jumping down the throats of people who don't understand the subject or who think they don't care, we should pay attention to every step that someone is taking towards privacy and congratulate it. You've quit Whatsapp for Signal? Some may say that signal is not the best, but it's a first step, congratulations for this change.

## References

[Snowden revelations \[a\]](https://en.wikipedia.org/wiki/Global_surveillance_disclosures_(2013%E2%80%93present))  
[Liberty definition by thefreedictionary.com \[b\]](https://www.thefreedictionary.com/liberty)  
[Privacy definition by Cambridge dictionary \[c\]](https://dictionary.cambridge.org/dictionary/english/privacy)  
[NSA Domestic Surveillance National Data Warehouse \[d\]](https://nsa.gov1.info/data/)
