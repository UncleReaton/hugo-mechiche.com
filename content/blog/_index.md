+++
title = "Blog posts"
sort_by = "date"
template = "posts_index.html"
page_template = "page-post.html"
generate_feed = true
transparent = true
paginate_by = 5
paginate_path = "page"
+++
